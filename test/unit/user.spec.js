const user = require('../../src/user');

it('user id number 1', () =>{
    expect(user(1)).toMatchSnapshot();
});

it('user id number 56', () =>{
    expect(user(56)).toMatchSnapshot();
});

it('user id number 1345', () =>{
    expect(user(1345)).toMatchSnapshot();
});

