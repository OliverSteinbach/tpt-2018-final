var config = require('../../nightwatch.conf.js');

module.exports = {
    'tptlive test': function(browser) {
        browser
            .resizeWindow(1920, 1080)
            .url('https://www.tptlive.ee/')
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'tptlive.png')
            .waitForElementVisible('body')
            .click(`#menu-item-1313`)            
            .saveScreenshot(config.imgpath(browser) + 'tunniplaan.png') 
            .waitForElementVisible('body')
            .click('a[href="https://tpt.siseveeb.ee/veebivormid/tunniplaan/tunniplaan?oppegrupp=226&nadal=03.12.2018"]')
            .saveScreenshot(config.imgpath(browser) + 'ta17e.png')
            .pause(2000)
            .end();

    }
};
